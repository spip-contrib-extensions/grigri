<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


function tables_grigri() {
	$T = array();
	foreach (lister_tables_objets_sql() as $k => $d) {
		if (is_array($d['field']) and array_key_exists('grigri', $d['field'])) {
			$T[] = $k;
		}
	}
	return $T;
}
