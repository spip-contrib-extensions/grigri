<?php
// This is a SPIP language file -- Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	// A
	'afficher_public_prive' => 'Configuration de l\'affichage de la saisie des grigri',
	'admins_autorises' => 'Autoriser les administrateurs',
	'admins_webmestres' => 'Admins / webmestres',
	
	// C
	'champ_grigri_label' => 'Grigri',
	'champ_grigri_explication' => 'Grigri  pour cet objet. Il s\'agit d\'un nom informatique : caractères alphanumériques ou «_».',
	'configuration_base' => 'Configuration de base',
	
	// E
	'explication_squelettes_prives' => 'Afficher la saisie du grigri dans les squelettes de l\'espace privé',
	'explication_squelettes_publics' => 'Afficher la saisie du grigri dans les squelettes des pages publiques',
	'explication_admins_autorises' => 'Par défaut seuls les webmestres peuvent créer/modifier/effacer les grigri',
	
	// S
	'squelettes_prives' => 'Espace privé',
	'squelettes_publics' => 'Pages publiques',
	
	// T
	'titre_page_configurer_grigri' => 'Configuration du plugin Grigri',

	// U
	'utilisateurs_autorises' => 'Utilisateurs autorisés',

);
